package logic;

import dto.GameDto;
import enums.Marker;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class GameLogicTest {

    private GameLogic gameLogic;

    @Before
    public void before() {
        this.gameLogic = new GameLogicImpl();
    }

    @Test
    public void createGameWithDefaultSize() {
        GameDto newGame = gameLogic.newGame();
        assertEquals(3, newGame.playingField.size());
        assertEquals(3, newGame.playingField.get(1).size());
        for (Map<Integer, Marker> row : newGame.playingField.values()) {
            assertTrue(row.values().stream().allMatch(v -> v == Marker.None));
        }
    }

    @Test
    public void createGameWithCustomSize() {
        int size = 15;
        GameDto newGame = gameLogic.newGame(size);
        assertEquals(size, newGame.playingField.size());
        assertEquals(size, newGame.playingField.get(1).size());
        for (Map<Integer, Marker> row : newGame.playingField.values()) {
            assertTrue(row.values().stream().allMatch(v -> v == Marker.None));
        }
    }

    @Test
    public void checkSetMarkerToField() {
        gameLogic.newGame();
        int x = 2;
        Marker expectedMarker = Marker.Circle;
        GameDto game = gameLogic.setMarker(x, x, expectedMarker);
        assertEquals(expectedMarker, game.playingField.get(x).get(x));
    }

    @Test
    public void checkFieldAlreadyPainted() {
        gameLogic.newGame();
        int x = 2;
        gameLogic.setMarker(x, x, Marker.Cross);
        try {
            gameLogic.setMarker(x, x, Marker.Cross);
        } catch (IllegalArgumentException e) {
            String expectedError = "Field 2|2 already marked by Cross";
            assertEquals(expectedError, e.getMessage());
        }
    }

    @Test
    public void checkSetNonMarkerOrNull() {
        gameLogic.newGame();
        int x = 2;
        gameLogic.setMarker(x, x, Marker.Cross);
        try {
            gameLogic.setMarker(x, x, Marker.None);
        } catch (IllegalArgumentException e) {
            String expectedError = "Wrong marker! Marker can by " + Marker.Cross + " or " + Marker.Circle;
            assertEquals(expectedError, e.getMessage());
        }
        try {
            gameLogic.setMarker(x, x, null);
        } catch (IllegalArgumentException e) {
            String expectedError = "Wrong marker! Marker can by " + Marker.Cross + " or " + Marker.Circle;
            assertEquals(expectedError, e.getMessage());
        }
    }

    @Test
    public void checkSetToWrongField() {
        gameLogic.newGame();
        int x = 15;
        try {
            gameLogic.setMarker(x, x, Marker.Cross);
        } catch (IllegalArgumentException e) {
            String expectedError ="Wrong field! Min field number = 1, max field number = 3";
            assertEquals(expectedError, e.getMessage());
        }
    }

    @Test
    public void trySetMarketBeforeCreatingGame() {
        try {
            gameLogic.setMarker(1, 1, Marker.Circle);
        } catch (IllegalArgumentException e){
            String expectedError = "Create new game first!";
            assertEquals(expectedError, e.getMessage());
        }
    }

    @Test
    public void getDiagonalWinnerWithDefaultGameSizeFromFirstRow() {
        gameLogic.newGame();
        Marker expectedWinner = Marker.Cross;

        gameLogic.setMarker(1, 1, expectedWinner);
        gameLogic.setMarker(2, 2, expectedWinner);
        GameDto game = gameLogic.setMarker(3, 3, expectedWinner);

        assertEquals(expectedWinner, game.winner);
    }

    @Test
    public void getDiagonalWinnerWithDefaultGameSizeFromLastRow() {
        gameLogic.newGame();
        Marker expectedWinner = Marker.Cross;

        gameLogic.setMarker(3, 1, expectedWinner);
        gameLogic.setMarker(2, 2, expectedWinner);
        GameDto game = gameLogic.setMarker(1, 3, expectedWinner);

        assertEquals(expectedWinner, game.winner);
    }


    @Test
    public void getRowWinnerWithDefaultGameSize() {
        gameLogic.newGame();
        Marker expectedWinner = Marker.Cross;

        gameLogic.setMarker(3, 1, expectedWinner);
        gameLogic.setMarker(3, 2, expectedWinner);
        GameDto game = gameLogic.setMarker(3, 3, expectedWinner);

        assertEquals(expectedWinner, game.winner);
    }

    @Test
    public void getColumnWinnerWithDefaultGameSize() {
        gameLogic.newGame();
        Marker expectedWinner = Marker.Cross;

        gameLogic.setMarker(1, 2, expectedWinner);
        gameLogic.setMarker(2, 2, expectedWinner);
        GameDto game = gameLogic.setMarker(3, 2, expectedWinner);

        assertEquals(expectedWinner, game.winner);
    }


    @Test
    public void getColumnWinnerIsNoneWithDefaultGameSize() {
        gameLogic.newGame();
        gameLogic.setMarker(1, 2, Marker.Cross);
        gameLogic.setMarker(2, 2, Marker.Cross);
        GameDto game = gameLogic.setMarker(3, 2, Marker.Circle);

        assertEquals(Marker.None, game.winner);
    }
}