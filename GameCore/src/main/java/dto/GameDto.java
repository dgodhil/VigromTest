package dto;

import enums.Marker;

import java.util.Map;

public class GameDto {

    public Map<Integer, Map<Integer, Marker>> playingField;
    public Marker winner;

    public GameDto() {
    }

    private GameDto(Map<Integer, Map<Integer, Marker>> playingField, Marker winner) {
        this.playingField = playingField;
        this.winner = winner;
    }

    public static GameDto mapGameInfoToDto(Map<Integer, Map<Integer, Marker>> playingField, Marker winner) {
        return new GameDto(playingField, winner);
    }
}
