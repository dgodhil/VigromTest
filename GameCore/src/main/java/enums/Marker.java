package enums;

public enum Marker {
    None,
    Cross,
    Circle
}
