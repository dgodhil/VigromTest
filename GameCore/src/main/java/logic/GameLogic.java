package logic;

import dto.GameDto;
import enums.Marker;

public interface GameLogic {

    /**
     * Создание игры с размером по умолчанию 3*3
     * @return информация о текущей игре
     */
    GameDto newGame();

    /**
     * Создание игры с заданием размера полей
     * @param size размер поля, значение по умолчанию 3
     * @return информация о текущей игре
     */
    GameDto newGame(int size);

    /**
     * Установка маркера в поле
     * @param fieldX строка
     * @param fieldY столбец
     * @param marker устанавливаемый маркер
     * @return информация о текущей игре
     */
    GameDto setMarker(final int fieldX, final int fieldY, final Marker marker);
}
