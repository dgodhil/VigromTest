package logic;

import dto.GameDto;
import enums.Marker;

public class GameLogicImpl implements GameLogic {

    private static final int FIRST_ROW = 1;
    private static final int DEFAULT_SIZE = 3;
    private GameInfo gameInfo;

    @Override
    public GameDto newGame() {
        gameInfo = new GameInfo(DEFAULT_SIZE);
        return GameDto.mapGameInfoToDto(gameInfo.getPlayingField(), Marker.None);
    }

    @Override
    public GameDto newGame(final int size) {
        if (size <= DEFAULT_SIZE) {
            return newGame();
        }

        gameInfo = new GameInfo(size);
        return GameDto.mapGameInfoToDto(gameInfo.getPlayingField(), Marker.None);
    }

    @Override
    public GameDto setMarker(final int fieldX, final int fieldY, final Marker marker) {
        validate(fieldX, fieldY, marker);
        gameInfo.setMarker(fieldX, fieldY, marker);

        return GameDto.mapGameInfoToDto(gameInfo.getPlayingField(), getWinner());
    }

    private void validate(final int fieldX, final int fieldY, final Marker marker) {
        if(gameInfo == null || gameInfo.getPlayingFieldSize() == 0) {
            throw new IllegalArgumentException("Create new game first!");
        }
        if (fieldX == 0 || fieldX > gameInfo.getPlayingFieldSize() || fieldY == 0 || fieldY > gameInfo.getPlayingFieldSize()) {
            throw new IllegalArgumentException("Wrong field! Min field number = 1, max field number = " + gameInfo.getPlayingField().size());
        }

        if (marker == null || marker == Marker.None) {
            throw new IllegalArgumentException("Wrong marker! Marker can by " + Marker.Cross + " or " + Marker.Circle);
        }

        Marker rowMarker = gameInfo.getPlayingField().get(fieldX).get(fieldY);
        if (rowMarker != Marker.None) {
            throw new IllegalArgumentException("Field " + fieldX + "|" + fieldY + " already marked by " + rowMarker);
        }
    }

    private Marker getWinner() {
        //выбор по диагонали [1.1],[2.2],[3.3]
        Marker winner = getDiagonalWinner(FIRST_ROW);
        if (isMarkerNonNone(winner)) {
            return winner;
        }

        //выбор по диагонали [3.1],[2.2],[1.3]
        winner = getDiagonalWinner(gameInfo.getPlayingFieldSize());
        if (isMarkerNonNone(winner)) {
            return winner;
        }

        //если по диагоналям не нашли победителя, то проверяем строки
        winner = getRowWinner();
        if (isMarkerNonNone(winner)) {
            return winner;
        }

        return getColumnWinner();
    }

    private Marker getDiagonalWinner(int firstCheckedField) {
        Marker winner;
        if (firstCheckedField == 1) {
            // проверка первой ячейки, если там не Marker.None, то проверяем диагональ
            winner = gameInfo.getPlayingField().get(FIRST_ROW).get(FIRST_ROW);
            if(isMarkerNonNone(winner)) {
                Marker fieldMarker;
                for (int i = 2; i <= gameInfo.getPlayingFieldSize(); i++) {
                    fieldMarker = gameInfo.getPlayingField().get(i).get(i);
                    if (fieldMarker != winner) {
                        return Marker.None;
                    }
                }
            }
            return winner;
        }
        //провека последей ячейки первого столбца, если там не Marker.None, то проверяем диагональ
        winner = gameInfo.getPlayingField().get(firstCheckedField).get(FIRST_ROW);
        if (isMarkerNonNone(winner)) {
            Marker fieldMarker;
            int fieldY = FIRST_ROW;
            for (int i = firstCheckedField - 1; i >= FIRST_ROW; i--) {
                fieldMarker = gameInfo.getPlayingField().get(i).get(++fieldY);
                if (fieldMarker != winner) {
                    return Marker.None;
                }
            }
        }

        return winner;
    }

    private Marker getRowWinner() {
        for (int i = FIRST_ROW; i <= gameInfo.getPlayingFieldSize(); i++) {
            Marker winner = gameInfo.getPlayingField().get(i).get(FIRST_ROW);
            if (!isMarkerNonNone(winner)) {
                continue;
            }
            if (gameInfo.getPlayingField().get(i).values().stream().allMatch(v -> v == winner)) {
                return winner;
            }
        }
        return Marker.None;
    }

    private Marker getColumnWinner(){
        for (int i = FIRST_ROW; i <= gameInfo.getPlayingFieldSize(); i++) {
            Marker winner = gameInfo.getPlayingField().get(FIRST_ROW).get(i);
            if (!isMarkerNonNone(winner)){
                continue;
            }
            int columnNumber = i;
            if (gameInfo.getPlayingField().entrySet().stream().map(f -> f.getValue().get(columnNumber)).allMatch(m -> m == winner)) {
                return winner;
            }
        }

        return Marker.None;
    }

    private boolean isMarkerNonNone(Marker marker) {
        return marker != Marker.None;
    }

}
