package logic;

import enums.Marker;

import java.util.HashMap;
import java.util.Map;

class GameInfo {

    private int playingFieldSize;
    private Map<Integer, Map<Integer, Marker>> playingField;

    GameInfo(int size) {
        this.playingFieldSize = size;
        init();
    }

    Map<Integer, Map<Integer, Marker>> getPlayingField() {
        return playingField == null || playingField.size() == 0
                ? new HashMap<>()
                : playingField;
    }

    int getPlayingFieldSize() {
        return playingFieldSize;
    }

    void setMarker(int fieldX, int fieldY, Marker marker) {
        playingField.get(fieldX).put(fieldY, marker);
    }

    private void init() {
        this.playingField = new HashMap<>(playingFieldSize);
        for (int i = 1; i <= playingFieldSize; i++) {
            playingField.put(i, initRow());
        }
    }

    private Map<Integer, Marker> initRow(){
        Map<Integer, Marker> row = new HashMap<>(playingFieldSize);
        for (int i = 1; i <= playingFieldSize; i++) {
            row.put(i, Marker.None);
        }
        return row;
    }

}
