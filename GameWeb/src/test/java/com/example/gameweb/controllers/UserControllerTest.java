package com.example.gameweb.controllers;

import com.example.gameweb.dto.AccountDto;
import com.example.gameweb.dto.UserDto;
import com.example.gameweb.entries.User;
import com.example.gameweb.repositories.UserRepository;
import com.google.gson.Gson;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest
public class UserControllerTest {

    private MediaType mediaTypeJson = new MediaType(MediaType.APPLICATION_JSON, Charset.defaultCharset());
    private MockMvc mockMvc;
    private HttpMessageConverter messageConverter;
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private WebApplicationContext webApplicationContext;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    private void setWebApplicationContext(WebApplicationContext webApplicationContext) {
        this.webApplicationContext = webApplicationContext;
    }

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {
        this.messageConverter = Arrays.stream(converters)
                .filter(c -> c instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.messageConverter);
    }

    @Before
    public void before() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        userRepository.deleteAllInBatch();
    }

    @After
    public void after() {
        userRepository.deleteAllInBatch();
    }

    @Test
    public void getAll() throws Exception {
        User user1 = createUser(new AccountDto("kek1", "kek_pass1"));
        User user2 = createUser(new AccountDto("kek2", "kek_pass2"));

        MvcResult result = mockMvc.perform(
                get("/users")
                )
                .andExpect(status().isOk())
                .andExpect(content().contentType(mediaTypeJson))
                .andReturn();

        String content = result.getResponse().getContentAsString();

        List<UserDto> response = Arrays.asList(new Gson().fromJson(content, UserDto[].class));
        response.sort(Comparator.comparing(u -> u.userId));
        assertEquals(2, response.size());
        assertEquals(user1.getId(), response.get(0).userId);
        assertEquals(user1.getLogin(), response.get(0).login);
        assertEquals(new ArrayList<>(), response.get(0).gameHistories);
        assertEquals(user2.getId(), response.get(1).userId);
        assertEquals(user2.getLogin(), response.get(1).login);
        assertEquals(new ArrayList<>(), response.get(1).gameHistories);
    }

    @Test
    public void create() throws Exception {
        AccountDto newUser = new AccountDto();
        newUser.login = "kek";
        newUser.password = "kek_test";
        mockMvc.perform(
                post("/users")
                        .content(json(newUser))
                        .contentType(mediaTypeJson)
                )
                .andExpect(status().isOk())
                .andExpect(content().contentType(mediaTypeJson))
                .andExpect(jsonPath("$.login", is(newUser.login)))
                .andExpect(jsonPath("$.password", is(newUser.password)));
    }

    @Test
    public void login() throws Exception {
        AccountDto accountDto = new AccountDto();
        accountDto.login = "kek";
        accountDto.password = "kek_test";

        User user = createUser(accountDto);

        mockMvc.perform(
                post("/users/login")
                        .content(json(accountDto))
                        .contentType(mediaTypeJson)
                )
                .andExpect(status().isOk())
                .andExpect(content().contentType(mediaTypeJson))
                .andExpect(jsonPath("$.userId", is(user.getId())))
                .andExpect(jsonPath("$.login", is(user.getLogin())));
    }

    private String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        messageConverter.write(o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

    private User createUser(AccountDto accountDto) {
        User user = new User();
        user.setLogin(accountDto.login);
        user.setPass(passwordEncoder.encode(accountDto.password));
        userRepository.save(user);
        return user;
    }
}