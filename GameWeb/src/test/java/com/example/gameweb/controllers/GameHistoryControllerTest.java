package com.example.gameweb.controllers;

import com.example.gameweb.dto.GameHistoryDto;
import com.example.gameweb.dto.GameInfoDto;
import com.example.gameweb.dto.UserDto;
import com.example.gameweb.entries.GameHistory;
import com.example.gameweb.entries.User;
import com.example.gameweb.repositories.GameHistoryRepository;
import com.example.gameweb.repositories.UserRepository;
import com.google.gson.Gson;
import dto.GameDto;
import enums.Marker;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;
import java.util.*;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest
public class GameHistoryControllerTest {

    private MediaType mediaTypeJson = new MediaType(MediaType.APPLICATION_JSON, Charset.defaultCharset());
    private MockMvc mockMvc;
    private HttpMessageConverter messageConverter;
    private UserRepository userRepository;
    private GameHistoryRepository gameHistoryRepository;
    private PasswordEncoder passwordEncoder;
    private WebApplicationContext webApplicationContext;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setGameHistoryRepository(GameHistoryRepository gameHistoryRepository) {
        this.gameHistoryRepository = gameHistoryRepository;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    private void setWebApplicationContext(WebApplicationContext webApplicationContext) {
        this.webApplicationContext = webApplicationContext;
    }

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {
        this.messageConverter = Arrays.stream(converters)
                .filter(c -> c instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.messageConverter);
    }

    @Before
    public void before() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        gameHistoryRepository.deleteAllInBatch();
        userRepository.deleteAllInBatch();
    }

    @After
    public void after() {
        gameHistoryRepository.deleteAllInBatch();
        userRepository.deleteAllInBatch();
    }

    @Test
    public void getUserHistory() throws Exception {
        User user = new User("history","history");
        userRepository.save(user);

        GameDto gameDto = new GameDto();
        gameDto.playingField = new HashMap<>();
        gameDto.winner = Marker.Cross;
        GameInfoDto gameInfoDto = new GameInfoDto(gameDto, Marker.Cross, null);
        GameHistory history1 = new GameHistory(user, gameInfoDto);
        gameHistoryRepository.save(history1);

        gameInfoDto = new GameInfoDto(gameDto, Marker.Circle, null);
        GameHistory history2 = new GameHistory(user, gameInfoDto);
        gameHistoryRepository.save(history2);

        MvcResult result = mockMvc.perform(
                get("/histories/user/" + user.getId())
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(mediaTypeJson))
                .andReturn();

        String content = result.getResponse().getContentAsString();

        List<GameHistoryDto> response = Arrays.asList(new Gson().fromJson(content, GameHistoryDto[].class));
        response.sort(Comparator.comparing(h -> h.id));
        assertEquals(2, response.size());
        assertEquals(user.getId(), response.get(0).userId);
        assertEquals(history1.getId(), response.get(0).id);
        assertEquals(history1.getUserMarker(), response.get(0).marker);
        assertTrue(response.get(0).isUserWinner);
        assertEquals(history2.getId(), response.get(1).id);
        assertEquals(history2.getUserMarker(), response.get(1).marker);
        assertFalse(response.get(1).isUserWinner);
    }
}
