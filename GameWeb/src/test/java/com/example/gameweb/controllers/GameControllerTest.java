package com.example.gameweb.controllers;

import com.example.gameweb.dto.GameInfoDto;
import com.example.gameweb.dto.NewGameDto;
import com.example.gameweb.dto.PlayingDto;
import com.example.gameweb.repositories.UserRepository;
import com.google.gson.Gson;
import enums.Marker;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest
public class GameControllerTest {


    private MediaType mediaTypeJson = new MediaType(MediaType.APPLICATION_JSON, Charset.defaultCharset());
    private MockMvc mockMvc;
    private HttpMessageConverter messageConverter;
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private WebApplicationContext webApplicationContext;
    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    private void setWebApplicationContext(WebApplicationContext webApplicationContext) {
        this.webApplicationContext = webApplicationContext;
    }

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {
        this.messageConverter = Arrays.stream(converters)
                .filter(c -> c instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.messageConverter);
    }

    @Before
    public void before() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        userRepository.deleteAll();
    }

    @Test
    public void newGameWithDefaultFieldSize() throws Exception {
        NewGameDto newGameDto = new NewGameDto(0, null, Marker.Circle);

        MvcResult result = mockMvc.perform(
                post("/games/new")
                        .content(json(newGameDto))
                        .contentType(mediaTypeJson)
                )
                .andExpect(status().isOk())
                .andExpect(content().contentType(mediaTypeJson))
                .andReturn();

        String content = result.getResponse().getContentAsString();
        GameInfoDto response = new Gson().fromJson(content, GameInfoDto.class);

        assertNull(response.userId);
        assertEquals(Marker.Circle, response.userMarker);
        assertEquals(Marker.None, response.winner);
        assertEquals(3, response.playingField.size());
        assertTrue(response.playingField
                .values()
                .stream()
                .allMatch(f -> f.values()
                        .stream()
                        .allMatch(r -> r == Marker.None))
        );
    }

    @Test
    public void newGameWithCustomFieldSize() throws Exception {
        NewGameDto newGameDto = new NewGameDto(17, null, Marker.Circle);

        MvcResult result = mockMvc.perform(
                post("/games/new")
                        .content(json(newGameDto))
                        .contentType(mediaTypeJson)
                )
                .andExpect(status().isOk())
                .andExpect(content().contentType(mediaTypeJson))
                .andReturn();

        String content = result.getResponse().getContentAsString();
        GameInfoDto response = new Gson().fromJson(content, GameInfoDto.class);

        assertNull(response.userId);
        assertEquals(Marker.Circle, response.userMarker);
        assertEquals(Marker.None, response.winner);
        assertEquals(17, response.playingField.size());
        assertTrue(response.playingField
                .values()
                .stream()
                .allMatch(f -> f.values()
                        .stream()
                        .allMatch(r -> r == Marker.None))
        );
    }

    @Test
    public void setMarker() throws Exception {
        Marker userMarker = Marker.Circle;
        NewGameDto newGameDto = new NewGameDto(0, null, userMarker);

        mockMvc.perform(
                post("/games/new")
                        .content(json(newGameDto))
                        .contentType(mediaTypeJson)
                )
                .andExpect(status().isOk())
                .andExpect(content().contentType(mediaTypeJson))
                .andReturn();

        PlayingDto playingDto = new PlayingDto(null, 1, 1, userMarker, userMarker);

        MvcResult result = mockMvc.perform(
                post("/games/pvp")
                        .content(json(playingDto))
                        .contentType(mediaTypeJson)
                )
                .andExpect(status().isOk())
                .andExpect(content().contentType(mediaTypeJson))
                .andReturn();

        String content = result.getResponse().getContentAsString();
        GameInfoDto response = new Gson().fromJson(content, GameInfoDto.class);

        assertNull(response.userId);
        assertEquals(Marker.Circle, response.userMarker);
        assertEquals(Marker.None, response.winner);
        assertEquals(userMarker, response.playingField.get(1).get(1));
    }

    @Test
    public void setMarkerAi() throws Exception {
        Marker userMarker = Marker.Circle;
        NewGameDto newGameDto = new NewGameDto(0, null, userMarker);

        mockMvc.perform(
                post("/games/new")
                        .content(json(newGameDto))
                        .contentType(mediaTypeJson)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(mediaTypeJson))
                .andReturn();

        PlayingDto playingDto = new PlayingDto(null, 1, 1, userMarker, userMarker);

        MvcResult result = mockMvc.perform(
                post("/games/ai")
                        .content(json(playingDto))
                        .contentType(mediaTypeJson)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(mediaTypeJson))
                .andReturn();

        String content = result.getResponse().getContentAsString();
        GameInfoDto response = new Gson().fromJson(content, GameInfoDto.class);

        assertNull(response.userId);
        assertEquals(Marker.Circle, response.userMarker);
        assertEquals(Marker.None, response.winner);

        List<Marker> aiMarkers = new ArrayList<>();
        for (Map<Integer, Marker> row : response.playingField.values()) {
            aiMarkers.addAll(row.values().stream().filter(marker -> marker != userMarker && marker != Marker.None).collect(Collectors.toList()));
        }

        assertEquals(1, aiMarkers.size());
        assertEquals(Marker.Cross, aiMarkers.get(0));
    }

    private String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        messageConverter.write(o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

}