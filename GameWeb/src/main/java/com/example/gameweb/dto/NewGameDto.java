package com.example.gameweb.dto;

import enums.Marker;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class NewGameDto {

    @ApiModelProperty(required = false, value = "Размер поля, по умолчанию 3")
    public int size;

    @ApiModelProperty(required = false, value = "Id пользователя")
    public Integer userId;

    @ApiModelProperty(required = true, value = "Маркер пользователя. Доступные значения Circle, Cross")
    public Marker marker;
}
