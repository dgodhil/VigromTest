package com.example.gameweb.dto;

import enums.Marker;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class PlayingDto {

    @ApiModelProperty(required = true, value = "Id пользователя")
    public Integer userId;

    @ApiModelProperty(required = true, value = "Поле по горизонтали")
    public int fieldX;

    @ApiModelProperty(required = true, value = "Поле по вертикали")
    public int fieldY;

    @ApiModelProperty(required = true, value = "Маркер устанавливаемый в поле. Доступные значения Circle, Cross")
    public Marker marker;

    @ApiModelProperty(required = true, value = "Маркер пользователя. Доступные значения Circle, Cross")
    public Marker playerMarker;
}
