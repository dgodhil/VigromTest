package com.example.gameweb.dto;

import com.example.gameweb.entries.GameHistory;
import enums.Marker;
import io.swagger.annotations.ApiModelProperty;
import org.joda.time.LocalDate;

public class GameHistoryDto {
    @ApiModelProperty(required = false, value = "Id истории игры")
    public long id;

    @ApiModelProperty(required = false, value = "Id пользователя")
    public long userId;

    @ApiModelProperty(required = false, value = "Фигура, которой играл пользователь. Доступные значения Circle, Cross")
    public Marker marker;

    @ApiModelProperty(required = false, value = "Пользователь выиграл")
    public boolean isUserWinner;

    public GameHistoryDto() {
    }

    public GameHistoryDto(GameHistory gameHistory) {
        this.id = gameHistory.getId();
        this.userId = gameHistory.getUser().getId();
        this.marker = gameHistory.getUserMarker();
        this.isUserWinner = gameHistory.isUserWinner();
    }
}
