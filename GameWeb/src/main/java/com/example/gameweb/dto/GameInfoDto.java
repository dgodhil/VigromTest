package com.example.gameweb.dto;

import dto.GameDto;
import enums.Marker;
import io.swagger.annotations.ApiModelProperty;

public class GameInfoDto extends GameDto {

    @ApiModelProperty(required = true, value = "Id пользователя")
    public Integer userId;

    @ApiModelProperty(required = true, value = "Маркер пользователя. Доступные значения Circle, Cross")
    public Marker userMarker;

    public GameInfoDto(GameDto gameDto, Marker userMarker, Integer userId) {
        this.playingField = gameDto.playingField;
        this.winner = gameDto.winner;
        this.userMarker = userMarker;
        if(userId != null) {
            this.userId = userId;
        }
    }
}
