package com.example.gameweb.dto;

import com.example.gameweb.entries.User;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserDto {

    @ApiModelProperty(required = false, value = "Id пользователя")
    public int userId;

    @ApiModelProperty(required = true, value = "Логин пользователя")
    public String login;

    @ApiModelProperty(required = true, value = "Пароль пользователя")
    public String password;

    @ApiModelProperty(required = false, value = "История игр")
    public List<GameHistoryDto> gameHistories = new ArrayList<>();

    public UserDto() {
    }

    public UserDto(User user) {
        this.userId = user.getId();
        this.login = user.getLogin();
        if (user.getGameHistories() != null) {
            this.gameHistories = user.getGameHistories().stream().map(GameHistoryDto::new).collect(Collectors.toList());
        }
    }

}
