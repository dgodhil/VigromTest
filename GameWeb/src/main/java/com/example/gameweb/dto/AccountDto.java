package com.example.gameweb.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class AccountDto {
    @ApiModelProperty(required = true, value = "Логин пользователя")
    public String login;

    @ApiModelProperty(required = true, value = "Пароль пользователя")
    public String password;
}
