package com.example.gameweb.controllers;

import com.example.gameweb.dto.GameInfoDto;
import com.example.gameweb.dto.NewGameDto;
import com.example.gameweb.dto.PlayingDto;
import com.example.gameweb.services.GameHistoryService;
import com.example.gameweb.services.GameService;
import enums.Marker;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/games")
public class GameController {

    private GameService gameService;
    private GameHistoryService gameHistoryService;

    @Autowired
    public void setGameService(GameService gameService) {
        this.gameService = gameService;
    }

    @Autowired
    public void setGameHistoryService(GameHistoryService gameHistoryService) {
        this.gameHistoryService = gameHistoryService;
    }

    @RequestMapping(method = RequestMethod.POST,  path = "/new")
    @ApiOperation(value = "newGame", notes = "Создание новой игры")
    public ResponseEntity<GameInfoDto> newGame(@RequestBody NewGameDto newGameDto){
        return ResponseEntity.ok(gameService.newGame(newGameDto));
    }

    @RequestMapping(method = RequestMethod.POST, path = "/pvp")
    @ApiOperation(value = "setMarker", notes = "Установка маркера в поле")
    public ResponseEntity<GameInfoDto> setMarker(@RequestBody PlayingDto playingDto) {
        GameInfoDto gameInfoDto = gameService.setMarker(playingDto);

        if (gameInfoDto.winner != Marker.None && gameInfoDto.userId != null) {
            gameHistoryService.saveGameToHistory(gameInfoDto);
        }

        return ResponseEntity.ok(gameInfoDto);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/ai")
    @ApiOperation(value = "setMarker", notes = "Установка маркера в поле")
    public ResponseEntity<GameInfoDto> setMarkerAi(@RequestBody PlayingDto playingDto) {
        GameInfoDto gameInfoDto = gameService.setMarkerWithAI(playingDto);

        if (gameInfoDto.winner != Marker.None && gameInfoDto.userId != null) {
            gameHistoryService.saveGameToHistory(gameInfoDto);
        }

        return ResponseEntity.ok(gameInfoDto);
    }
}
