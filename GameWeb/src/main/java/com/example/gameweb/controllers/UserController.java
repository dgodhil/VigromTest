package com.example.gameweb.controllers;

import com.example.gameweb.dto.AccountDto;
import com.example.gameweb.dto.UserDto;
import com.example.gameweb.services.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "getAll", notes = "Запрос списка всех пользователей")
    public ResponseEntity<List<UserDto>> getAll() {
        return ResponseEntity.ok(userService.getAll());
    }

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "create", notes = "Создание нового пользователя")
    public ResponseEntity<UserDto> create(@RequestBody AccountDto newUser) {
        return ResponseEntity.ok(userService.createNew(newUser));
    }

    @RequestMapping(method = RequestMethod.POST, path = "/login")
    @ApiOperation(value = "login", notes = "Вход")
    public ResponseEntity<UserDto> login(@RequestBody AccountDto user) {
        return ResponseEntity.ok(userService.login(user));
    }

}
