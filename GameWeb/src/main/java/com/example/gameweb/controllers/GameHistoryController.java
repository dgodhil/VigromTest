package com.example.gameweb.controllers;

import com.example.gameweb.dto.GameHistoryDto;
import com.example.gameweb.services.GameHistoryService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/histories")
public class GameHistoryController {

    private GameHistoryService gameHistoryService;

    @Autowired
    public void setGameHistoryService(GameHistoryService gameHistoryService) {
        this.gameHistoryService = gameHistoryService;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/user/{userId}")
    @ApiOperation(value = "getUserHistory", notes = "Запрос списка историй игр пользователя")
    public ResponseEntity<List<GameHistoryDto>> getUserHistory(@PathVariable int userId) {
        return ResponseEntity.ok(gameHistoryService.getUserHistory(userId));
    }
}
