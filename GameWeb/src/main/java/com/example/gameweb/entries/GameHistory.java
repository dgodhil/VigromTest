package com.example.gameweb.entries;

import com.example.gameweb.dto.GameInfoDto;
import enums.Marker;
import lombok.Getter;
import lombok.Setter;
import org.joda.time.LocalDate;

import javax.persistence.*;

@Entity
@Table(name = "GameHistory")
@Getter
@Setter
public class GameHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    //@Type(type = "org.joda.time.contrib.hibernate.PersistentLocalDate")
    @Column(name = "finish_at")
    private LocalDate finishAt;

    @Enumerated(EnumType.STRING)
    @Column
    private Marker userMarker;

    @Column(name = "is_user_winner")
    private boolean isUserWinner;

    public GameHistory() {
    }

    public GameHistory(User user, GameInfoDto gameInfoDto) {
        this.user = user;
        this.finishAt = LocalDate.now();
        this.userMarker = gameInfoDto.userMarker;
        this.isUserWinner = gameInfoDto.userMarker == gameInfoDto.winner;
    }
}
