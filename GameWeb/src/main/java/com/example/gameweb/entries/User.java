package com.example.gameweb.entries;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Users")
@Getter
@Setter
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(unique = true)
    private String login;

    @Column
    private String pass;

    @Column(name = "game_histories")
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<GameHistory> gameHistories;

    public User() {
    }

    public User(String login, String pass) {
        this.login = login;
        this.pass = pass;
    }
}
