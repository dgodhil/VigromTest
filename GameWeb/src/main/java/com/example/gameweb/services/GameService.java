package com.example.gameweb.services;

import com.example.gameweb.dto.GameInfoDto;
import com.example.gameweb.dto.NewGameDto;
import com.example.gameweb.dto.PlayingDto;

public interface GameService {

    GameInfoDto newGame(NewGameDto newGameDto);

    GameInfoDto setMarker(PlayingDto playingDto);

    GameInfoDto setMarkerWithAI(PlayingDto playingDto);
}
