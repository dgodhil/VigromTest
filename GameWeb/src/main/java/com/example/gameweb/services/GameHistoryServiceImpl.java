package com.example.gameweb.services;

import com.example.gameweb.dto.GameHistoryDto;
import com.example.gameweb.dto.GameInfoDto;
import com.example.gameweb.entries.GameHistory;
import com.example.gameweb.repositories.GameHistoryRepository;
import com.example.gameweb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GameHistoryServiceImpl implements GameHistoryService {

    private GameHistoryRepository gameHistoryRepository;
    private UserRepository userRepository;

    @Autowired
    public void setGameHistoryRepository(GameHistoryRepository gameHistoryRepository) {
        this.gameHistoryRepository = gameHistoryRepository;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<GameHistoryDto> getUserHistory(int userId) {
        return gameHistoryRepository.findAllByUserId(userId).stream().map(GameHistoryDto::new).collect(Collectors.toList());
    }

    @Override
    public void saveGameToHistory(GameInfoDto gameInfoDto) {
        userRepository.getById(gameInfoDto.userId)
                .ifPresent(user1 -> gameHistoryRepository.save(new GameHistory(user1, gameInfoDto)));
    }
}
