package com.example.gameweb.services;

import com.example.gameweb.dto.GameHistoryDto;
import com.example.gameweb.dto.GameInfoDto;

import java.util.List;

public interface GameHistoryService {

    List<GameHistoryDto> getUserHistory(int userId);

    void saveGameToHistory(GameInfoDto gameInfoDto);
}
