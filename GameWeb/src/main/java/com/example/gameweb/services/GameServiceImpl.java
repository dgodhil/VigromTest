package com.example.gameweb.services;

import com.example.gameweb.dto.GameInfoDto;
import com.example.gameweb.dto.NewGameDto;
import com.example.gameweb.dto.PlayingDto;
import dto.GameDto;
import enums.Marker;
import logic.GameLogic;
import logic.GameLogicImpl;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class GameServiceImpl implements GameService {

    private GameLogic game;

    public GameServiceImpl() {
        this.game = new GameLogicImpl();
    }

    @Override
    public GameInfoDto newGame(NewGameDto newGameDto) {
        GameInfoDto gameDto = new GameInfoDto(
                game.newGame(newGameDto.size),
                newGameDto.marker,
                newGameDto.userId);
        if (newGameDto.userId != null) {
            gameDto.userId = newGameDto.userId;
        }
        return gameDto;
    }

    @Override
    public GameInfoDto setMarker(PlayingDto playingDto) {
        GameInfoDto gameDto = new GameInfoDto(
                game.setMarker(playingDto.fieldX, playingDto.fieldY, playingDto.marker),
                playingDto.playerMarker,
                playingDto.userId);

        if(playingDto.userId != null) {
            gameDto.userId = playingDto.userId;
        }
        return gameDto;
    }

    @Override
    public GameInfoDto setMarkerWithAI(PlayingDto playingDto) {
        //ход игрока
        GameDto gameDto = game.setMarker(playingDto.fieldX, playingDto.fieldY, playingDto.marker);

        if (gameDto.winner != Marker.None) {
            return new GameInfoDto(gameDto, playingDto.playerMarker, playingDto.userId);
        }
        //ход AI
        PlayingDto aiDto = new PlayingDto();
        aiDto.marker = Marker.Circle == playingDto.playerMarker ? Marker.Cross : Marker.Circle;
        getAiFields(gameDto, aiDto, playingDto.playerMarker);
        gameDto = game.setMarker(aiDto.fieldX, aiDto.fieldY, aiDto.marker);

        return new GameInfoDto(gameDto, playingDto.playerMarker, playingDto.userId);
    }

    private void getAiFields(GameDto gameDto, PlayingDto aiDto, Marker playerMarker) {
        List<Pair<Integer, Integer>> emptyFields = new ArrayList<>();
        for (int i = 1; i <= gameDto.playingField.size(); i++) {
            for (int j = 1; j <= gameDto.playingField.size(); j++) {
                if (gameDto.playingField.get(i).get(j) != playerMarker && gameDto.playingField.get(i).get(j) != aiDto.marker) {
                    emptyFields.add(Pair.of(i, j));
                }
            }
        }
        if (!emptyFields.isEmpty()) {
            int field = ThreadLocalRandom.current().nextInt(0, emptyFields.size());
            aiDto.fieldX = emptyFields.get(field).getFirst();
            aiDto.fieldY = emptyFields.get(field).getSecond();
        }
    }
}
