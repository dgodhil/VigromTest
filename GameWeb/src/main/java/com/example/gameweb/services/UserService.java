package com.example.gameweb.services;

import com.example.gameweb.dto.GameHistoryDto;
import com.example.gameweb.dto.AccountDto;
import com.example.gameweb.dto.UserDto;

import java.util.List;

public interface UserService {

    List<UserDto> getAll();

    UserDto getById(int userId);

    UserDto createNew(AccountDto accountDto);

    List<GameHistoryDto> getUserGameHistory(int userId);

    UserDto getFullUserInfo(int userId);

    UserDto login(AccountDto accountDto);
}
