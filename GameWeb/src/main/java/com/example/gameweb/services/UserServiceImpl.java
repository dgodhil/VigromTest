package com.example.gameweb.services;

import com.example.gameweb.dto.GameHistoryDto;
import com.example.gameweb.dto.AccountDto;
import com.example.gameweb.dto.UserDto;
import com.example.gameweb.entries.User;
import com.example.gameweb.exceptions.ResourceNotFoundException;
import com.example.gameweb.repositories.GameHistoryRepository;
import com.example.gameweb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private PasswordEncoder passwordEncoder;
    private UserRepository userRepository;
    private GameHistoryRepository gameHistoryRepository;
    private final Supplier<ResourceNotFoundException> userNotFoundSupplier = () -> new ResourceNotFoundException("User not found");

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setGameHistoryRepository(GameHistoryRepository gameHistoryRepository) {
        this.gameHistoryRepository = gameHistoryRepository;
    }

    @Override
    public List<UserDto> getAll() {
        List<User> users = userRepository.findAll();
        if (users.size() == 0) {
            return new ArrayList<>();
        }
        return users.stream().map(UserDto::new).collect(Collectors.toList());
    }

    @Override
    public UserDto getById(int userId) {
        User user = userRepository.getById(userId).orElseThrow(userNotFoundSupplier);
        return new UserDto(user);
    }

    @Override
    public UserDto createNew(AccountDto userDto) {
        User user = new User();
        user.setLogin(userDto.login);
        user.setPass(passwordEncoder.encode(userDto.password));
        userRepository.save(user);

        UserDto result = new UserDto(user);
        result.password = userDto.password;
        return result;
    }

    @Override
    public List<GameHistoryDto> getUserGameHistory(int userId) {
        return gameHistoryRepository.findAllByUserId(userId).stream().map(GameHistoryDto::new).collect(Collectors.toList());
    }

    @Override
    public UserDto getFullUserInfo(int userId) {
        User user = userRepository.getById(userId).orElseThrow(userNotFoundSupplier);
        return new UserDto(user);
    }

    @Override
    public UserDto login(AccountDto accountDto) {
        User user = userRepository.getByLogin(accountDto.login)
                .orElseThrow(userNotFoundSupplier);
        if (passwordEncoder.matches(accountDto.password, user.getPass())) {
            return new UserDto(user);
        }
        return null;
    }

}
