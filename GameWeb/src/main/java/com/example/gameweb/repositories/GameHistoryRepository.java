package com.example.gameweb.repositories;

import com.example.gameweb.entries.GameHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GameHistoryRepository extends JpaRepository<GameHistory, Integer> {

    Optional<GameHistory> getById(int id);

    List<GameHistory> findAllByUserId(int userId);

}
