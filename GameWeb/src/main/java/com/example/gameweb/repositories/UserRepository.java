package com.example.gameweb.repositories;

import com.example.gameweb.entries.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

        Optional<User> getById(int id);

        Optional<User> getByLogin(String login);
}